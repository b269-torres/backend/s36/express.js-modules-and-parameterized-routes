// Contains instructions on HOW API will perform its intended tasks
// All the operations it can do will be place in this file

const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

//controller function for creating a task
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name:requestBody.name

	})

	return newTask.save().then((task, error) => {
		if (error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
};

//controller function for deleting a task

// Route to delete task
/*
Business Logic
Look for the task with the corresponding id provided in the URL/route
Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/
		
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((
		removedTask, err) => {
		if (err){
			console.log(err);
			return false;
		} else {
			return "Deleted Task."
		}
	})
}

//Activity
module.exports.getSpecificTasks = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((
		specificTask, err) => {
		if (err){
			console.log(err);
			return false;
		} else {
			return result
		}
	})
}
